import React from 'react';
// provides a collection of easing functions that can be used to create smooth animations.
import { easing } from 'maath';
// state management library for React that provides a simple way to manage global state in a performant way.
import { useSnapshot } from "valtio";
// A React renderer for the Three.js library that allows for declarative 3D graphics and animations in React.
import { useFrame} from "@react-three/fiber";
// glTF is a standard file format for three-dimensional scenes and models.
// Decal (贴花) is a component for adding decals to a 3D object
// useTexture is a hook for loading textures for use in 3D graphics.
import { Decal, useGLTF, useTexture } from "@react-three/drei";
import state from '../store';

const Shirt = () => {
    const snap = useSnapshot(state)
    const { nodes, materials } = useGLTF('/shirt_baked.glb')
    const logoTexture = useTexture(snap.logoDecal)
    const fullTexture = useTexture(snap.fullDecal)

    useFrame((state,delta) => easing.dampC(materials.lambert1.color, snap.color, 0.25, delta));

    // sometimes the shirt will not update, we need to create a key with the string of snap
    const stateString = JSON.stringify(snap)

    return (
        // whenever stateString changes, the component will re-render
        <group key={stateString}>
            <mesh
                castShadow={true}
                geometry={nodes.T_Shirt_male.geometry}
                material={materials.lambert1}
                material-roughness={1}
                dispose={null}
            >
                {snap.isFullTexture && (
                    <Decal
                        position={[0, 0, 0]}
                        rotation={[0, 0, 0]}
                        scale={1}
                        map={fullTexture}
                    />
                )}
                {snap.isLogoTexture && (
                    <Decal
                        position={[0, 0.04, 0.15]}
                        rotation={[0, 0, 0]}
                        scale={0.15}
                        map={logoTexture}
                        map-anisotropy={16}
                        depthTest={false}
                        depthWrite={true}
                    />
                )}
            </mesh>
        </group>
    );
};

export default Shirt;